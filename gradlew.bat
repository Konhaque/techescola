<?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android" android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:background="#1d2733">
   <ScrollView
       android:layout_width="match_parent"
       android:layout_height="match_parent">
     <LinearLayout
         android:layout_width="match_parent"
         android:layout_height="wrap_content"
         android:orientation="vertical">
         <ImageView
             android:layout_width="match_parent"
             android:layout_height="wrap_content"
             android:src="@drawable/bt_amarelo"/>
         <TextView
             android:layout_width="match_parent"
             android:layout_height="wrap_content"
             android:text="SINAL AMARELO"
             android:layout_marginTop="20dp"
             android:textAlignment="center"
             android:textColor="#dfba08"
             android:textSize="20dp"/>
         <TextView
             android:layout_width="match_parent"
             android:layout_height="wrap_content"
             android:text="FIQUE EM CASA"
             android:textAlignment="center"
             android:layout_marginTop="10dp"
             android:id="@+id/casa"
             android:textSize="25dp"
             />
         <TextView
             android:layout_width="match_parent"
             android:layout_height="wrap_content"
             android:text="Com base nas suas informações
e nos protocolos oficiais dos orgãos de saúde, a recomendação é de que
''FIQUE ALERTA EM CASA''. Redobre os cuidados com
a sua saúde, o isolamento social e higiene das mãos. Ao sair de casa use
máscaras e evite aglomerações. Proteja a sua família!"
             android:layout_marginTop="20dp"
             android:textAlignment="center"
             android:layout_marginLeft="40dp"
             android:layout_marginRight="40dp"
             android:textSize="23dp"
             android:textColor="#4a617d"
             />
         <Button
             android:layout_width="match_parent"
             android:layout_height="60dp"
             android:layout_marginRight="70dp"
             android:layout_marginLeft="