package com.techescola;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.techescola.escolheCadastro.sou_aluno;
import com.techescola.escolheCadastro.sou_professor;
import com.techescola.escolheCadastro.SlidePage;

import java.util.ArrayList;
import java.util.List;

public class Transicao_Cadastro extends AppCompatActivity {
    private ViewPager pager;
    private PagerAdapter adapter;
    private List<Fragment> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullscream();
        setContentView(R.layout.activity_transicao__cadastro);
        inicia_objetos();
        passatela();
    }

    private void inicia_objetos(){
        list = new ArrayList<>();
        list.add(new sou_aluno());
        list.add(new sou_professor());
        pager = (ViewPager) findViewById(R.id.vpage);
        adapter = new SlidePage(getSupportFragmentManager(), list);
    }

    private void passatela(){
        pager.setAdapter(adapter);
        guia(list.size(),0);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                guia(list.size(),position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void guia(int tam, int pos){
        //ArrayList<String> arrayList = new ArrayList<>();
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.botoes);
        linearLayout.removeAllViews();
        for(int i = 0; i<tam; i++){
            TextView textView = new TextView(this);
            textView.setText(getText(R.string.doted));
            textView.setTextSize(35f);
            if(i == pos) textView.setTextColor(Color.parseColor("#01cd87"));
            else textView.setTextColor(Color.parseColor("#3a5f8b"));
            linearLayout.addView(textView);
        }
    }

    private void fullscream(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View tela = getWindow().getDecorView();
        tela.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN+View.SYSTEM_UI_FLAG_HIDE_NAVIGATION+View.SYSTEM_UI_FLAG_IMMERSIVE);
    }


}
