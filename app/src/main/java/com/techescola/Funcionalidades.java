package com.techescola;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.techescola.funcionalidades.Comunidade;
import com.techescola.funcionalidades.Materias;
import com.techescola.funcionalidades.Noticias;
import com.techescola.funcionalidades.Notificacao;

public class Funcionalidades extends AppCompatActivity {
    private BottomNavigationView menu;
    private BottomNavigationView.OnNavigationItemSelectedListener itemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    Fragment fragment = null;
                    switch (item.getItemId()){
                        case R.id.notificacao:
                            fragment = new Notificacao();
                            abreTela(fragment);
                            return true;
                        case R.id.materias:
                            fragment = new Materias();
                            abreTela(fragment);
                            return true;
                        case R.id.comunidade:
                            fragment = new Comunidade();
                            abreTela(fragment);
                            return true;
                        case R.id.noticia:
                            fragment = new Noticias();
                            abreTela(fragment);
                            return true;
                    }
                    return false;
                }
            };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullscream();
        setContentView(R.layout.activity_funcionalidades);
        menu = (BottomNavigationView) findViewById(R.id.nav_bar);
        menu.setOnNavigationItemSelectedListener(itemSelectedListener);
        menu.setSelectedItemId(R.id.materias);
    }

    private void fullscream(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View tela = getWindow().getDecorView();
        tela.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN+View.SYSTEM_UI_FLAG_HIDE_NAVIGATION+View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void abreTela(Fragment fragment){
        getSupportFragmentManager().beginTransaction().setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out)
                .replace(R.id.tela,fragment).commit();
    }
}