package com.techescola;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.Repo;
import com.techescola.bd.Aluno;
import com.techescola.bd.DisciplinaProfessor;
import com.techescola.bd.Disciplinas;
import com.techescola.bd.Professor;
import com.techescola.bd.Repository;

import java.util.List;

public class Login extends AppCompatActivity {

    private TextView btnCad;
    private Button login;
    private TextView btnTrocaPerfil;
    private EditText email;
    private EditText senha;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullscream();
        setContentView(R.layout.activity_login);
        iniciaObjetos();
        cadastrar();
        logar();
        mudar_Perfil();
    }

    private void iniciaObjetos(){
        btnCad = (TextView) findViewById(R.id.btnCad);
        login = (Button) findViewById(R.id.btLogin);
        btnTrocaPerfil = (TextView) findViewById(R.id.trocarPerfil);
        email = (EditText) findViewById(R.id.usuario);
        senha = (EditText) findViewById(R.id.senha);
    }

    private void cadastrar(){
        btnCad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Registro.class);
                intent.putExtra(new Repository(Login.this).getPerfil(),new Repository(Login.this).getPerfil());
                startActivity(intent);
                finish();
            }
        });
    }
    private void fullscream(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View tela = getWindow().getDecorView();
        tela.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN+View.SYSTEM_UI_FLAG_HIDE_NAVIGATION+View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void logar(){
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alerta = new AlertDialog.Builder(Login.this);
                LayoutInflater layoutInflater = getLayoutInflater();
                alerta.setView(layoutInflater.inflate(R.layout.carregando,null));
                alerta.setCancelable(true);
                dialog = alerta.create();
                dialog.show();

             if(new Repository(Login.this).getPerfil().equalsIgnoreCase("aluno")) setLoginALUnos();
             else setLoginProfessor();
            }
        });
    }

    private void mudar_Perfil(){
        btnTrocaPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this,Transicao_Cadastro.class));
                finish();
            }
        });
    }

    private void setLoginALUnos(){
        FirebaseDatabase.getInstance().goOnline();
        DatabaseReference bd = FirebaseDatabase.getInstance().getReference("Alunos");
        bd.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                boolean existe = false;
                for(DataSnapshot ds: snapshot.getChildren()){
                    if(ds.child("aluno").child("email").getValue().toString().equalsIgnoreCase(email.getText().toString())
                    && ds.child("aluno").child("senha").getValue().toString().equals(senha.getText().toString())){
                        existe = true;
                        new Repository(Login.this).criarAluno();
                        Aluno aluno = new Aluno();
                        aluno.setNome(ds.child("aluno").child("nome").getValue().toString());
                        aluno.setEmail(ds.child("aluno").child("email").getValue().toString());
                        aluno.setEscola(ds.child("aluno").child("escola").getValue().toString());
                        aluno.setMatricula(ds.child("aluno").child("matricula").getValue().toString());
                        aluno.setSerie(ds.child("aluno").child("serie").getValue().toString());
                        aluno.setTipo(ds.child("aluno").child("tipo").getValue().toString());
                        new Repository(Login.this).salvaAluno(aluno,ds.getKey());
                        Intent intent = new Intent(Login.this,Funcionalidades.class);
                        startActivity(intent);
                        finish();
                        break;
                    }
                }
                if(!existe){
                    dialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                    builder.setTitle("Ocorreu um erro ao fazer login.");
                    builder.setMessage("Verifique seu email e sua senha e tente novamente!");
                    builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    AlertDialog alerta;
                    alerta = builder.create();
                    alerta.show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void setLoginProfessor(){
        FirebaseDatabase.getInstance().goOnline();
        DatabaseReference bd = FirebaseDatabase.getInstance().getReference("Professores");
        bd.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                GenericTypeIndicator<List<DisciplinaProfessor>> disciplas = new GenericTypeIndicator<List<DisciplinaProfessor>>() {
                };
                boolean existe = false;
                for(DataSnapshot ds: snapshot.getChildren()){
                    if(ds.child("email").getValue().toString().equalsIgnoreCase(email.getText().toString())
                       && ds.child("senha").getValue().toString().equals(senha.getText().toString())) {
                        existe = true;
                        new Repository(Login.this).criarProfessor();
                        Professor professor = new Professor();
                        professor.setNome(ds.child("nome").getValue().toString());
                        professor.setEmail(ds.child("email").getValue().toString());
                        professor.setEstado(ds.child("estado").getValue().toString());
                        professor.setTelefone(ds.child("telefone").getValue().toString());
                        new Repository(Login.this).salvaProfessor(professor);
                        new Repository(Login.this).criaDisciplinaProfessor();
                        List<DisciplinaProfessor> disciplinas = ds.child("disciplinas").getValue(disciplas);
                        for(int i = 0; i<disciplinas.size();i++){
                            DisciplinaProfessor disciplinaProfessor = new DisciplinaProfessor();
                            disciplinaProfessor.setDisciplina(disciplinas.get(i).getDisciplina());
                            disciplinaProfessor.setSerie(disciplinas.get(i).getSerie());
                            new Repository(Login.this).salvaDisciplinaProfessor(disciplinaProfessor);
                        }
                        Intent intent = new Intent(Login.this,Funcionalidades.class);
                        startActivity(intent);
                        finish();
                        break;
                    }
                }
                if(!existe){
                    dialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                    builder.setTitle("Ocorreu um erro ao fazer login.");
                    builder.setMessage("Verifique seu email e sua senha e tente novamente!");
                    builder.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    AlertDialog alerta;
                    alerta = builder.create();
                    alerta.show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }




}
