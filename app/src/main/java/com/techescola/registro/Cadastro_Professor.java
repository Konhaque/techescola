package com.techescola.registro;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.techescola.Escolhe_Disciplina;
import com.techescola.R;
import com.techescola.bd.Professor;
import com.techescola.bd.Repository;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Cadastro_Professor extends Fragment {
    private Spinner sexo;
    private EditText cep;
    private EditText nome;
    private EditText email;
    private EditText senha;
    private EditText confirmarSenha;
    private Spinner cidade;
    private Spinner estado;
    private EditText logradouro;
    private EditText bairro;
    private EditText numero;
    private EditText telefone;
    private EditText cpf;
    private ArrayList<String> estados;
    private ArrayList<String> cidades;
    private Button avancar;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.tela_cad_professor, container, false);
        return viewGroup;
    }

    @Override
    public void onStart() {
        iniciaObjetos();
        setSexo();
        buscacep();
        setEstado(estados);
        setCidade(cidades);
        setAvancar();
        super.onStart();
    }

    private void iniciaObjetos(){
        sexo = getActivity().findViewById(R.id.sexo);
        cep = (EditText) getActivity().findViewById(R.id.cep);
        nome = (EditText) getActivity().findViewById(R.id.nome);
        email = (EditText) getActivity().findViewById(R.id.email);
        senha = (EditText) getActivity().findViewById(R.id.senha);
        confirmarSenha = (EditText) getActivity().findViewById(R.id.confirma_senha);
        cidade = (Spinner) getActivity().findViewById(R.id.cidade);
        estado = (Spinner) getActivity().findViewById(R.id.estado);
        logradouro = (EditText) getActivity().findViewById(R.id.logradouro);
        bairro = (EditText) getActivity().findViewById(R.id.bairro);
        numero = (EditText) getActivity().findViewById(R.id.numero);
        telefone = (EditText) getActivity().findViewById(R.id.telefone);
        avancar = (Button) getActivity().findViewById(R.id.btnAvancar);
        cpf = (EditText) getActivity().findViewById(R.id.cpf);
        estados = new ArrayList<>();
        estados.add("Estados");
        cidades = new ArrayList<>();
        cidades.add("Cidades");
    }

    private void setAvancar(){
        avancar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                carregarProfessor();
                startActivity(new Intent(getActivity(), Escolhe_Disciplina.class));
                getActivity().finish();
            }
        });
    }

    private void carregarProfessor(){
        new Repository(getContext()).criarProfessor();
        Professor professor = new Professor();
        professor.setNome(nome.getText().toString());
        professor.setCpf(cpf.getText().toString());
        professor.setEmail(email.getText().toString());
        professor.setSenha(senha.getText().toString());
        professor.setCep(cep.getText().toString());
        professor.setEstado(estado.getSelectedItem().toString());
        professor.setCidade(cidade.getSelectedItem().toString());
        professor.setEndereco(logradouro.getText().toString());
        professor.setNumero(numero.getText().toString());
        professor.setBairro(bairro.getText().toString());
        professor.setTelefone(telefone.getText().toString());
        professor.setSexo(sexo.getSelectedItem().toString());
        new Repository(getContext()).salvaProfessor(professor);
    }

    private void setSexo(){
        ArrayAdapter arraySexo = ArrayAdapter.createFromResource(getContext(),R.array.sexo,R.layout.spinner_item_professor);
        arraySexo.setDropDownViewResource(R.layout.spinner_dropdown);
        sexo.setAdapter(arraySexo);
    }

    private void setEstado(ArrayList<String> estados){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),R.layout.spinner_item_professor,estados);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown);
        estado.setAdapter(adapter);
    }

    private void setCidade(ArrayList<String> cidades){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),R.layout.spinner_item_professor,cidades);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown);
        cidade.setAdapter(adapter);
    }


    private void buscacep(){
        cep.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(cep.length() == 8) new procuraCep().execute();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private class procuraCep extends AsyncTask<Void,Void,Void>{
        JSONObject jsonObject;
        AlertDialog dialog;
        @Override
        protected void onPreExecute() {
           AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
            LayoutInflater layoutInflater = getActivity().getLayoutInflater();
            alerta.setView(layoutInflater.inflate(R.layout.carregando,null));
            alerta.setCancelable(true);
            dialog = alerta.create();
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            estados.clear();
            cidades.clear();
            try{
                logradouro.setText(jsonObject.getString("logradouro"));
                bairro.setText(jsonObject.getString("bairro"));
                estados.add(jsonObject.getString("uf"));
                cidades.add(jsonObject.getString("localidade"));
                setEstado(estados);
                setCidade(cidades);
            }catch (JSONException e){
                e.getMessage();
            }

            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            cep();
            return null;
        }

        private void cep(){
            try{
                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder().url("http://viacep.com.br/ws/"+cep.getText().toString()+"/json/")
                        .method("GET",null)
                        .build();
                Response response = client.newCall(request).execute();
                String as = response.body().string();
                try{
                     jsonObject = new JSONObject(as);
                }catch (JSONException e){
                    e.getMessage();
                }
            }catch (IOException e){
                e.getMessage();
            }
        }
    }
}
