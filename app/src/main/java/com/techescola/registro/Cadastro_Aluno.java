package com.techescola.registro;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.core.Repo;
import com.techescola.Funcionalidades;
import com.techescola.R;
import com.techescola.bd.Aluno;
import com.techescola.bd.Repository;
import com.techescola.bd.SalvarFireBase;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Cadastro_Aluno extends Fragment {
    private EditText nome;
    private EditText dt_Nascimento;
    private EditText matricula;
    private EditText email;
    private EditText senha;
    private EditText confirma_Senha;
    private EditText cep;
    private Spinner cidade;
    private Spinner estado;
    private Spinner sexo;
    private EditText logradouro;
    private EditText bairro;
    private EditText numero;
    private EditText telefone;
    private EditText cpf;
    private ArrayList<String> estados;
    private ArrayList<String> cidades;
    private EditText escola;
    private Spinner series;
    private Button salvar;
    private Aluno aluno;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.tela_cad_aluno,container,false);
        return viewGroup;
    }

    @Override
    public void onStart() {
        inicia_Objetos();
        carregaSeries();
        setSexo();
        buscacep();
        setEstado(estados);
        setCidade(cidades);
        gerarMatricula();
        setSalvar();
        super.onStart();
    }

    private void inicia_Objetos(){
        aluno = new Aluno();
        nome = (EditText) getActivity().findViewById(R.id.nome_cad_aluno);
        //dt_Nascimento = (EditText) getActivity().findViewById(R.id.data_nasc);
        matricula = (EditText) getActivity().findViewById(R.id.num_matricula);
        cpf = (EditText) getActivity().findViewById(R.id.cpf);
        email = (EditText) getActivity().findViewById(R.id.email_cad_aluno);
        senha = (EditText) getActivity().findViewById(R.id.cad_passwd_aluno);
        confirma_Senha = (EditText) getActivity().findViewById(R.id.cad_passwd_aluno_confirm);
        cep = (EditText) getActivity().findViewById(R.id.cep);
        estado = (Spinner) getActivity().findViewById(R.id.estado);
        cidade = (Spinner) getActivity().findViewById(R.id.cidade);
        logradouro = (EditText) getActivity().findViewById(R.id.logradouro);
        numero = (EditText) getActivity().findViewById(R.id.numero);
        bairro = (EditText) getActivity().findViewById(R.id.bairro);
        sexo = (Spinner) getActivity().findViewById(R.id.cad_aluno_sexo);
        telefone =(EditText) getActivity().findViewById(R.id.telefone);
        escola = (EditText) getActivity().findViewById(R.id.cad_escola_aluno);
        series = (Spinner) getActivity().findViewById(R.id.cad_aluno_spinner);
        salvar = (Button) getActivity().findViewById(R.id.btnAvancar);
        estados = new ArrayList<>();
        estados.add("Estados");
        cidades = new ArrayList<>();
        cidades.add("Cidades");
    }

    private void setSexo(){
        ArrayAdapter arraySexo = ArrayAdapter.createFromResource(getContext(),R.array.sexo,R.layout.spinner_item);
        arraySexo.setDropDownViewResource(R.layout.spinner_dropdown);
        sexo.setAdapter(arraySexo);
    }

    private void carregaSeries(){
        ArrayAdapter<CharSequence> serie = ArrayAdapter.createFromResource(getContext(),R.array.series,R.layout.spinner_item);
        serie.setDropDownViewResource(R.layout.spinner_dropdown);
        series.setAdapter(serie);
    }

    private void setEstado(ArrayList<String> estados){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),R.layout.spinner_item,estados);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown);
        estado.setAdapter(adapter);
    }

    private void setCidade(ArrayList<String> cidades){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),R.layout.spinner_item,cidades);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown);
        cidade.setAdapter(adapter);
    }

    private void setAluno(){
        aluno.setNome(nome.getText().toString());
        aluno.setCpf(cpf.getText().toString());
        aluno.setSexo(sexo.getSelectedItem().toString());
        aluno.setEmail(email.getText().toString());
        aluno.setSenha(senha.getText().toString());
        aluno.setCep(cep.getText().toString());
        aluno.setEstado(estado.getSelectedItem().toString());
        aluno.setCidade(cidade.getSelectedItem().toString());
        aluno.setLogradouro(logradouro.getText().toString());
        aluno.setBairro(bairro.getText().toString());
        aluno.setEscola(escola.getText().toString());
        aluno.setMatricula(matricula.getText().toString());
        aluno.setSexo(sexo.getSelectedItem().toString());
        aluno.setTelefone(telefone.getText().toString());
        aluno.setTipo("Aluno");
        aluno.setSerie(series.getSelectedItem().toString());
    }

    private void setSalvar(){
            FirebaseDatabase.getInstance().goOnline();
            final DatabaseReference db = FirebaseDatabase.getInstance().getReference("Alunos");
            salvar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(verifica()) {
                        AlertDialog dialog;
                        AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
                        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
                        alerta.setView(layoutInflater.inflate(R.layout.carregando,null));
                        alerta.setCancelable(true);
                        dialog = alerta.create();
                        dialog.show();
                        setAluno();
                       final String id = db.push().getKey();
                        db.child(id).setValue(new SalvarFireBase(aluno)).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                new Repository(getContext()).criarAluno();
                                new Repository(getContext()).salvaAluno(aluno,id);
                                startActivity(new Intent(getActivity(), Funcionalidades.class));
                                getActivity().finish();
                            }
                        });
                    }
                }
            });


    }

    private void gerarMatricula(){
        cpf.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(cpf.getText().length() == 11){
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
                    Date data = new Date();
                    String ff = dateFormat.format(data);
                    matricula.setText(cpf.getText().toString()+ff);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private boolean verifica(){
        boolean salvar = false;
        if(nome.getText().length() == 0){
            nome.setError("Este campo precisa ser preenchido");
            nome.requestFocus();
        }else if(cpf.getText().length() == 0){
            cpf.setError("Este campo precisa ser preenchido");
            cpf.requestFocus();
        }else if(sexo.getSelectedItem().toString().equalsIgnoreCase("Sexo")){
            sexo.requestFocus();
        }else if(email.getText().length() == 0){
            email.setError("Este campo precisa ser preenchido");
            email.requestFocus();
        }else if(senha.getText().length() == 0){
            senha.setError("Este campo precisa ser preenchido");
            senha.requestFocus();
        }else if(!confirma_Senha.getText().toString().equalsIgnoreCase(senha.getText().toString())){
            confirma_Senha.setError("Os campos devem coincidir");
            confirma_Senha.requestFocus();
        }else if(cep.getText().length() == 0){
            cep.setError("Este campo precisa ser preenchido");
            cep.requestFocus();
        }else if(logradouro.getText().length() == 0){
            logradouro.setError("Este campo precisa ser preenchido");
            logradouro.requestFocus();
        }else if(bairro.getText().length() == 0){
            bairro.setError("Este campo precisa ser preenchido");
            bairro.requestFocus();
        }else if(escola.getText().length() == 0){
            escola.setError("Este campo precisa ser preenchido");
            escola.requestFocus();
        }else if(series.getSelectedItem().toString().equalsIgnoreCase("Série")){
            series.requestFocus();
        }else{
            salvar =  true;
        }

        return salvar;
    }



    private void buscacep(){
        cep.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(cep.length() == 8) new procuraCep().execute();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private class procuraCep extends AsyncTask<Void,Void,Void> {
        JSONObject jsonObject;
        AlertDialog dialog;
        @Override
        protected void onPreExecute() {
            AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
            LayoutInflater layoutInflater = getActivity().getLayoutInflater();
            alerta.setView(layoutInflater.inflate(R.layout.carregando,null));
            alerta.setCancelable(true);
            dialog = alerta.create();
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            estados.clear();
            cidades.clear();
            try{
                logradouro.setText(jsonObject.getString("logradouro"));
                bairro.setText(jsonObject.getString("bairro"));
                estados.add(jsonObject.getString("uf"));
                cidades.add(jsonObject.getString("localidade"));
                setEstado(estados);
                setCidade(cidades);
            }catch (JSONException e){
                e.getMessage();
            }

            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            cep();
            return null;
        }

        private void cep(){
            try{
                OkHttpClient client = new OkHttpClient.Builder().build();
                Request request = new Request.Builder().url("http://viacep.com.br/ws/"+cep.getText().toString()+"/json/")
                        .method("GET",null)
                        .build();
                Response response = client.newCall(request).execute();
                String as = response.body().string();
                try{
                    jsonObject = new JSONObject(as);
                }catch (JSONException e){
                    e.getMessage();
                }
            }catch (IOException e){
                e.getMessage();
            }
        }
    }
}
