package com.techescola;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.techescola.registro.Cadastro_Aluno;
import com.techescola.registro.Cadastro_Professor;

public class Registro extends AppCompatActivity {
    private Intent intent;
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullscream();
        setContentView(R.layout.activity_registro);
        tela();
    }

    private void fullscream(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View tela = getWindow().getDecorView();
        tela.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN+View.SYSTEM_UI_FLAG_HIDE_NAVIGATION+View.SYSTEM_UI_FLAG_IMMERSIVE);
    }
    private void tela(){
        intent = getIntent();
        if(intent.hasExtra("Aluno")){
           fragment = new Cadastro_Aluno();
           abre_Tela();
        }else{
            fragment = new Cadastro_Professor();
            abre_Tela();
        }
    }

    private void abre_Tela(){
        getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
    }
}