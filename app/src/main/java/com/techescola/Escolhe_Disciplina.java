package com.techescola;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.AsyncTaskLoader;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.fonts.FontFamily;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.techescola.bd.DisciplinaProfessor;
import com.techescola.bd.Disciplinas;
import com.techescola.bd.Professor;
import com.techescola.bd.Repository;
import com.techescola.bd.SalvarFireBase;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class Escolhe_Disciplina extends AppCompatActivity {
    private ArrayList<String> setDip;
    private Spinner series;
    private Spinner disciplina;
    private Button adicionar;
    private LinearLayout container;
    private Button salvar;
    private String tipo;
    private List<DisciplinaProfessor> disciplinaProfessors;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullscream();
        setContentView(R.layout.activity_escolhe__disciplina);
        inicia_Objetos();
        setSerie();
        disdks();
        setAdicionar();
        salvar();
    }

    private void setAdicionar(){
        adicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView = new TextView(Escolhe_Disciplina.this);
                textView.setText(disciplina.getSelectedItem().toString() + " - "+series.getSelectedItem().toString());
                textView.setTextColor(Color.WHITE);
                container.addView(textView);
                DisciplinaProfessor a = new DisciplinaProfessor();
                a.setSerie(series.getSelectedItem().toString());
                a.setDisciplina(disciplina.getSelectedItem().toString());
                disciplinaProfessors.add(a);
            }
        });
    }

    private void fullscream(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View tela = getWindow().getDecorView();
        tela.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN+View.SYSTEM_UI_FLAG_HIDE_NAVIGATION+View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void inicia_Objetos(){
        setDip = new ArrayList<>();
        disciplinaProfessors = new ArrayList<>();
        tipo = "";
        series = (Spinner) findViewById(R.id.serie);
        disciplina = (Spinner) findViewById(R.id.disciplina_prof);
        adicionar = (Button) findViewById(R.id.btnAdicionar);
        container = (LinearLayout) findViewById(R.id.mSelecionadas);
        salvar = (Button) findViewById(R.id.btnSalvar);
    }

    private void setSerie(){
        ArrayAdapter<CharSequence> serie = ArrayAdapter.createFromResource(this,R.array.series,R.layout.spinner_item_professor);
        serie.setDropDownViewResource(R.layout.spinner_dropdown);
        series.setAdapter(serie);
    }

    private void setDisciplina(){
        ArrayAdapter<String> disciplinas = new ArrayAdapter<String>(this,R.layout.spinner_item_professor,setDip);
        disciplinas.setDropDownViewResource(R.layout.spinner_dropdown);
        disciplina.setAdapter(disciplinas);
    }

    private void disdks(){
        series.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                procura_Disciplina();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void procura_Disciplina(){
        if(series.getSelectedItemPosition() > 4){
            tipo = "Medio";
            new procuraDisciplia().execute();
        }
        else if(series.getSelectedItemPosition() < 4 && series.getSelectedItemPosition() != 0){
            tipo = "Fundamental";
            new procuraDisciplia().execute();
        }
        /*tipo = "Medio";
        new procuraDisciplia().execute();*/
    }

    private void salvar(){
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog;
                AlertDialog.Builder alerta = new AlertDialog.Builder(Escolhe_Disciplina.this);
                LayoutInflater layoutInflater = getLayoutInflater();
                alerta.setView(layoutInflater.inflate(R.layout.carregando,null));
                alerta.setCancelable(true);
                dialog = alerta.create();
                dialog.show();
              FirebaseDatabase.getInstance().goOnline();
              DatabaseReference db = FirebaseDatabase.getInstance().getReference("Professores");
              db.child(db.push().getKey()).setValue(carrgaProfessor()).addOnSuccessListener(new OnSuccessListener<Void>() {
                  @Override
                  public void onSuccess(Void aVoid) {
                      salvarDisciplinas();
                      startActivity(new Intent(Escolhe_Disciplina.this, Funcionalidades.class));
                      finish();
                  }
              });
            }
        });
    }

    private Professor carrgaProfessor(){
        List<Professor> professors = new Repository(Escolhe_Disciplina.this).getProfessor();
        Professor professor = new Professor();
        professor.setNome(professors.get(0).getNome());
        professor.setDisciplinas(disciplinaProfessors);
        professor.setCpf(professors.get(0).getCpf());
        professor.setEmail(professors.get(0).getEmail());
        professor.setSenha(professors.get(0).getSenha());
        professor.setCep(professors.get(0).getCep());
        professor.setEstado(professors.get(0).getEstado());
        professor.setCidade(professors.get(0).getCidade());
        professor.setEndereco(professors.get(0).getEndereco());
        professor.setNumero(professors.get(0).getNumero());
        professor.setBairro(professors.get(0).getBairro());
        professor.setTelefone(professors.get(0).getTelefone());
        professor.setSexo(professors.get(0).getSexo());
        return professor;
    }

    private void salvarDisciplinas(){
        new Repository(this).criaDisciplinaProfessor();
        for(int i = 0; i<disciplinaProfessors.size();i++){
            DisciplinaProfessor disciplinaProfessor = new DisciplinaProfessor();
            disciplinaProfessor.setSerie(disciplinaProfessors.get(i).getSerie());
            disciplinaProfessor.setDisciplina(disciplinaProfessors.get(i).getDisciplina());
            new Repository(this).salvaDisciplinaProfessor(disciplinaProfessor);
        }
    }

    private class procuraDisciplia extends AsyncTask<Void,Void,Void>{
        AlertDialog dialog;
        private List<Disciplinas> dip = new ArrayList<>();
        private GenericTypeIndicator<List<Disciplinas>> disciplas = new GenericTypeIndicator<List<Disciplinas>>() {
        };
        @Override
        protected void onPreExecute() {
            FirebaseDatabase.getInstance().goOnline();
            AlertDialog.Builder alerta = new AlertDialog.Builder(Escolhe_Disciplina.this);
            LayoutInflater layoutInflater = getLayoutInflater();
            alerta.setView(layoutInflater.inflate(R.layout.carregando,null));
            alerta.setCancelable(true);
            dialog = alerta.create();
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            /*for(int i = 0; i<dip.size();i++){
                setDip.add(dip.get(i).getDisciplina());
            }*/
            //setDisciplina();
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            busca();
            return null;
        }

        private void busca(){
            DatabaseReference db = FirebaseDatabase.getInstance().getReference("Disciplinas");
            db.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    Log.i("fsdnfksbfks",snapshot.getValue()+"");
                    if(tipo.equalsIgnoreCase("Fundamental")){
                        dip = snapshot.child("Fundamental").child("disciplinas").getValue(disciplas);
                        Log.i("fsdnfksbfks",snapshot.child("Fundamental").child("disciplinas").getValue()+"");
                        carrega();
                    }else{
                        dip = snapshot.child("Medio").child("disciplinas").getValue(disciplas);
                        Log.i("fsdnfksbfks",dip.size()+"");
                        carrega();
                    }

                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        private void carrega(){
            setDip.clear();
            for(int i = 0; i<dip.size();i++){
                setDip.add(dip.get(i).getDisciplina());
            }
            setDisciplina();
        }

    }


}






