package com.techescola.funcionalidades;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.techescola.Mostra_Noticia;
import com.techescola.R;
import com.techescola.bd.InfoNoticias;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Noticias extends Fragment {
    private LinearLayout noticias;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.noticias, container, false);
        return  viewGroup;
    }

    @Override
    public void onStart() {
        iniciaObjetos();
        new RetornaNoticias().execute();
        super.onStart();
    }

    private void iniciaObjetos(){
        noticias = (LinearLayout) getActivity().findViewById(R.id.LLNoticia);
    }



    private class RetornaNoticias extends AsyncTask<Void,Void,Void>{
        private List<InfoNoticias> noticiasList;
        private AlertDialog dialog;
        @Override
        protected void onPreExecute() {
            noticiasList = new ArrayList<>();
            AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
            LayoutInflater layoutInflater = getActivity().getLayoutInflater();
            alerta.setView(layoutInflater.inflate(R.layout.carregando,null));
            alerta.setCancelable(true);
            dialog = alerta.create();
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            imprimeNoticias();
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            buscaNoticias();
            return null;
        }

        private void buscaNoticias(){
            try {
                final String url = "https://www.google.com/search?biw=1366&bih=657&tbm=nws&ei=96rpXsexOIvR5OUPttqogAw&q=educa%C3%A7%C3%A3o+brasil&oq=educa%C3%A7%C3%A3o+br&gs_l=psy-ab.3.0.0l2.22951.24736.0.25899.3.3.0.0.0.0.233.529.0j2j1.3.0....0...1c.1.64.psy-ab..0.3.525....0.MpCOWbOfQpc";
                Document doc = Jsoup.connect(url).get();
                Elements data = doc.select("div.g");
                for (int i = 0; i< data.size();i++){
                    InfoNoticias noticias = new InfoNoticias();
                    noticias.setTitulo(data.select("h3.r.dO0Ag").eq(i).text());
                    noticias.setImagem(data.select("img.th.BbeB2d").eq(i).attr("src"));
                    noticias.setDescricao(data.select("div.st").eq(i).text());
                    noticias.setFonte(data.select("div.dhIWPd").eq(i).text());
                    noticias.setLink(data.select("a.l.lLrAF").eq(i).attr("href"));
                    Log.i("llfsldfsl", noticias.getLink());
                    noticiasList.add(noticias);
                }
            }catch (IOException e){
                e.getMessage();
            }
        }

        private void imprimeNoticias(){
            dialog.dismiss();
            noticias.removeAllViews();
            for(int i = 0; i<noticiasList.size(); i++){
                LinearLayout linearLayout = new LinearLayout(getContext());
                LinearLayout ll2 = new LinearLayout(getContext());
                LinearLayout ll3 = new LinearLayout(getContext());
                ImageView imagem = new ImageView(getContext());
                TextView titulo = new TextView(getContext());
                TextView descricao = new TextView(getContext());
                TextView fonte = new TextView(getContext());
                LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params2.setMargins(20,10,20,10);
                linearLayout.setLayoutParams(params2);
                linearLayout.setOrientation(LinearLayout.VERTICAL);
                linearLayout.setBackground(getActivity().getDrawable(R.drawable.container_noticias));
                linearLayout.setPadding(0,20,0,0);
                imagem.setPadding(10, 10, 10, 10);
                Glide.with(getActivity()).load(noticiasList.get(i).getImagem()).into(imagem);
                ll2.addView(imagem, 100, 100);
                titulo.setPadding(20, 0, 0, 0);
                titulo.setTextSize(20f);
                titulo.setText(noticiasList.get(i).getTitulo());
                final int finalI = i;
                titulo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), Mostra_Noticia.class);
                        intent.putExtra("link", noticiasList.get(finalI).getLink());
                        intent.putExtra("titulo",noticiasList.get(finalI).getTitulo());
                        startActivity(intent);
                    }
                });
                titulo.setTextColor(Color.BLACK);
                ll2.setGravity(Gravity.CENTER);
                ll2.addView(titulo);
                ll3.setOrientation(LinearLayout.VERTICAL);
                ll3.setGravity(Gravity.LEFT);
                descricao.setPadding(10, 10, 10, 10);
                descricao.setTextSize(10f);
                descricao.setText(noticiasList.get(i).getDescricao());
                descricao.setTextColor(Color.BLACK);
                ll3.addView(descricao);
                fonte.setPadding(0, 20, 0, 10);
                fonte.setText(noticiasList.get(i).getFonte());
                fonte.setTextSize(10f);
                fonte.setTextColor(Color.BLACK);
                fonte.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                ll3.addView(fonte);
                noticias.setPadding(0, 20, 0, 10);
                linearLayout.addView(ll2);
                linearLayout.addView(ll3);
                noticias.addView(linearLayout);
            }
        }
    }

}
