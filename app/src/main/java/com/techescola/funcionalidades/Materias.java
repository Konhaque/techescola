package com.techescola.funcionalidades;

import android.app.AlertDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.Repo;
import com.techescola.Escolhe_Disciplina;
import com.techescola.Login;
import com.techescola.R;
import com.techescola.bd.Aluno;
import com.techescola.bd.DisciplinaProfessor;
import com.techescola.bd.Disciplinas;
import com.techescola.bd.Professor;
import com.techescola.bd.Repository;

import java.util.ArrayList;
import java.util.List;

public class Materias extends Fragment {
    private LinearLayout linearLayout;
    private String tipo = "";
    private List<Disciplinas> dip = new ArrayList<>();
    private List<Aluno> alunoList;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.materias,container,false);
        return viewGroup;
    }

    @Override
    public void onStart() {
        linearLayout = (LinearLayout) getActivity().findViewById(R.id.linear);
        verifica();
        super.onStart();
    }

    private void verifica(){
       if(new Repository(getContext()).getPerfil().equalsIgnoreCase("aluno")){
           alunoList = new Repository(getContext()).getAluno();
           if(alunoList.get(0).getSerie().equalsIgnoreCase("1° Ano") ||
           alunoList.get(0).getSerie().equalsIgnoreCase("2° Ano") ||
           alunoList.get(0).getSerie().equalsIgnoreCase("3° Ano")){
               tipo = "Medio";
           }else{
               tipo = "Fundamental";
           }
           new procuraDisciplia().execute();
        }else{
           List<DisciplinaProfessor> disciplinaProfessors = new Repository(getContext()).getDisciplinaProfessor();
           printaDProf(disciplinaProfessors);
       }
    }

    private void printaDisciplinas(){
        linearLayout.removeAllViews();
        TextView titulo = new TextView(getContext());
        titulo.setText("Disciplinas Matriculadas - Série: "+alunoList.get(0).getSerie());
        titulo.setLayoutParams(new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        titulo.setTextColor(Color.BLACK);
        titulo.setGravity(Gravity.CENTER);
        titulo.setTextSize(20f);
        linearLayout.addView(titulo);
        for(int i = 0; i< dip.size(); i++){
            LinearLayout linearLayout1 = new LinearLayout(getContext());
            LinearLayout ll2 = new LinearLayout(getContext());
            TextView textView = new TextView(getContext());
            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params2.setMargins(20,20,20,10);
            linearLayout1.setLayoutParams(params2);
            linearLayout1.setOrientation(LinearLayout.VERTICAL);
            linearLayout1.setBackground(getActivity().getDrawable(R.drawable.container_noticias));
            linearLayout1.setPadding(0,20,0,0);
            textView.setPadding(20, 0, 0, 0);
            textView.setTextSize(20f);
            textView.setText(dip.get(i).getDisciplina());
            textView.setTextColor(Color.BLACK);
            ll2.addView(textView);
            linearLayout1.addView(ll2);
            linearLayout.addView(linearLayout1);
        }
    }

    private void printaDProf(List<DisciplinaProfessor> dip){
        linearLayout.removeAllViews();
        TextView titulo = new TextView(getContext());
        titulo.setText("Disciplinas Ministradas");
        titulo.setLayoutParams(new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        titulo.setTextColor(Color.BLACK);
        titulo.setGravity(Gravity.CENTER);
        titulo.setTextSize(20f);
        linearLayout.addView(titulo);
        for(int i = 0; i< dip.size(); i++){
            LinearLayout linearLayout1 = new LinearLayout(getContext());
            LinearLayout ll2 = new LinearLayout(getContext());
            TextView textView = new TextView(getContext());
            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params2.setMargins(20,20,20,10);
            linearLayout1.setLayoutParams(params2);
            linearLayout1.setOrientation(LinearLayout.VERTICAL);
            linearLayout1.setBackground(getActivity().getDrawable(R.drawable.container_noticias));
            linearLayout1.setPadding(0,20,0,0);
            textView.setPadding(20, 0, 0, 0);
            textView.setTextSize(20f);
            textView.setText(dip.get(i).getDisciplina()+ " - "+dip.get(i).getSerie());
            textView.setTextColor(Color.BLACK);
            ll2.setGravity(Gravity.CENTER);
            ll2.addView(textView);
            linearLayout1.addView(ll2);
            linearLayout.addView(linearLayout1);
        }
    }


    private class procuraDisciplia extends AsyncTask<Void,Void,Void> {
        AlertDialog dialog;
        private GenericTypeIndicator<List<Disciplinas>> disciplas = new GenericTypeIndicator<List<Disciplinas>>() {
        };
        @Override
        protected void onPreExecute() {
            FirebaseDatabase.getInstance().goOnline();
            AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
            LayoutInflater layoutInflater = getLayoutInflater();
            alerta.setView(layoutInflater.inflate(R.layout.carregando,null));
            alerta.setCancelable(true);
            dialog = alerta.create();
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            /*for(int i = 0; i<dip.size();i++){
                setDip.add(dip.get(i).getDisciplina());
            }*/
            //setDisciplina();
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            busca();
            return null;
        }

        private void busca(){
            DatabaseReference db = FirebaseDatabase.getInstance().getReference("Disciplinas");
            db.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    Log.i("fsdnfksbfks",snapshot.getValue()+"");
                    if(tipo.equalsIgnoreCase("Fundamental")){
                        dip = snapshot.child("Fundamental").child("disciplinas").getValue(disciplas);
                        printaDisciplinas();
                    }else{
                        dip = snapshot.child("Medio").child("disciplinas").getValue(disciplas);
                        printaDisciplinas();
                        Log.i("fsdnfksbfks",dip.size()+"");
                    }

                }
                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }

    }

}

