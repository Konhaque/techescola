package com.techescola.funcionalidades;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.techescola.R;
import com.techescola.bd.Aluno;
import com.techescola.bd.Repository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Notificacao extends Fragment {
    private TextView textView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.notificacao,container,false);
        return viewGroup;
    }

    @Override
    public void onStart() {
        textView = (TextView) getActivity().findViewById(R.id.labee);
        //new dale().execute();
        super.onStart();
    }


    private class dale extends AsyncTask<Void,Void,Void>{
        JSONObject json;
        AlertDialog dialog;
        @Override
        protected void onPreExecute() {
            AlertDialog.Builder alerta = new AlertDialog.Builder(getContext());
            LayoutInflater layoutInflater = getActivity().getLayoutInflater();
            alerta.setView(layoutInflater.inflate(R.layout.carregando,null));
            alerta.setCancelable(true);
            dialog = alerta.create();
            dialog.show();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            dialog.dismiss();
            /*try {
                //textView.setText(json.getString("nome"));
            }catch (JSONException e){
                e.getMessage();
            }*/
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            pesquisar();
            return null;
        }

        private void pesquisar(){
            try {
                OkHttpClient client = new OkHttpClient().newBuilder()
                        .build();
                Request request = new Request.Builder()
                        .url("http://10.0.0.108:8080/disciplinas")
                        .method("GET", null)
                        .build();
                Response response = client.newCall(request).execute();
                String as = response.body().string();
                Log.i("kfhsffhs",as);
                try {
                    JSONArray array = new JSONArray(as);
                    Log.i("dljadljadl", array.getJSONObject(0).getString("descricao"));
                }catch (JSONException e){
                    Log.i("dljadljadl", e.getMessage());
                    e.getMessage();
                }

            }catch (IOException e){
                e.getMessage();
            }
        }
    }
}
