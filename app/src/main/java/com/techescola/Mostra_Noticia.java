package com.techescola;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class Mostra_Noticia extends AppCompatActivity {
    private WebView webView;
    private Intent intent;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullscream();
        setContentView(R.layout.activity_mostra__noticia);
        iniciaObjetos();
        setWebView();
        setToolbar();
    }

    private void fullscream(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View tela = getWindow().getDecorView();
        tela.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN+View.SYSTEM_UI_FLAG_HIDE_NAVIGATION+View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void iniciaObjetos(){
        webView = (WebView) findViewById(R.id.mostranoticia);
        intent = getIntent();
        toolbar = (Toolbar) findViewById(R.id.bar);
    }

    private void setToolbar(){
        toolbar.setTitle(intent.getStringExtra("titulo"));
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setWebView(){
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl(intent.getStringExtra("link"));
    }
}