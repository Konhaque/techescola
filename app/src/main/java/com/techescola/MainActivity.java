package com.techescola;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fullscream();
        setContentView(R.layout.activity_main);
        verifica();
    }

    private void mudar_tela(final Intent intent){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        },3000);
    }

    private void fullscream(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        View tela = getWindow().getDecorView();
        tela.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN+View.SYSTEM_UI_FLAG_HIDE_NAVIGATION+View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    private void verifica(){
        if(getSharedPrefs(this,"Techescola","ini") != null){
            mudar_tela(new Intent(this,Login.class));
        }else{
            setSharedPrefs(this,"Techescola","ini","foi");
            mudar_tela(new Intent(this, Transicao_Cadastro.class));
        }
    }

    private void setSharedPrefs(Context contexto,
                                String nomeProjeto,
                                String chave,
                                String valor) {
        SharedPreferences sharedPreferences;
        sharedPreferences = contexto.getSharedPreferences(nomeProjeto, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(chave, valor);
        editor.apply();

    }

    private String getSharedPrefs(Context contexto,
                                  String nomeProjeto,
                                  String chave) {
        SharedPreferences sharedPreferences;
        sharedPreferences = contexto.getSharedPreferences(nomeProjeto, Context.MODE_PRIVATE);
        return sharedPreferences.getString(chave, null);
    }


}
