package com.techescola.escolheCadastro;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.techescola.Login;
import com.techescola.R;
import com.techescola.Registro;
import com.techescola.bd.Repository;

public class sou_aluno extends Fragment {
    Button avancar;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.sou_aluno,container,false);
        return viewGroup;
    }

    @Override
    public void onStart() {
        inicia_Objetos();
        setAvancar();
        super.onStart();
    }

    private void inicia_Objetos(){
        avancar = getActivity().findViewById(R.id.btnAvancarAluno);
    }

    private void setAvancar(){
        avancar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Login.class);
                new Repository(getContext()).criarPerfil();
                new Repository(getContext()).salvaPerfil("Aluno");
                startActivity(intent);
                getActivity().finish();
            }
        });
    }
}
