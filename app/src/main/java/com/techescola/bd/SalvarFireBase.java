package com.techescola.bd;

import java.lang.reflect.Array;
import java.util.List;

public class SalvarFireBase {
    private Aluno aluno;
    private List<Disciplinas> disciplinas;
    private Professor professor;


    public SalvarFireBase(Professor professor){
        this.professor = professor;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public SalvarFireBase(Aluno aluno){
        this.aluno = aluno;
    }

    public SalvarFireBase(List<Disciplinas> disciplinas){
        this.disciplinas = disciplinas;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public List<Disciplinas> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplinas> disciplinas) {
        this.disciplinas = disciplinas;
    }





}
