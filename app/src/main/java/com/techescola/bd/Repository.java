package com.techescola.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

public class Repository {
    DaoUtil databaseutil;

    public Repository(Context context){
        databaseutil = new DaoUtil(context);
    }

    public void criarPerfil(){
        String Sql = "DROP TABLE IF EXISTS TB_Perfil";
        databaseutil.getConexaoDataBase().execSQL(Sql);
        StringBuilder sqlddl = new StringBuilder();
        sqlddl.append("CREATE TABLE TB_Perfil (");
        sqlddl.append("ID_PERFIL INTEGER PRIMARY KEY AUTOINCREMENT,");
        sqlddl.append("Perfil   TEXT)");
        databaseutil.getConexaoDataBase().execSQL(sqlddl.toString());
    }

    public void criarAluno(){
        String sql = "DROP TABLE IF EXISTS TB_ALUNO";
        databaseutil.getConexaoDataBase().execSQL(sql);
        StringBuilder sqlddl = new StringBuilder();
        sqlddl.append("CREATE TABLE TB_ALUNO(");
        sqlddl.append("ID_ALUNO TEXT NOT NULL,");
        sqlddl.append("Nome TEXT NOT NULL, ");
        sqlddl.append("Email TEXT NOT NULL, ");
        sqlddl.append("Escola TEXT NOT NULL, ");
        sqlddl.append("Matricula TEXT NOT NULL, ");
        sqlddl.append("Serie TEXT NOT NULL, ");
        sqlddl.append("Tipo TEXT NOT NULL)");
        databaseutil.getConexaoDataBase().execSQL(sqlddl.toString());
    }

    public void criarProfessor(){
        String sql = "DROP TABLE IF EXISTS TB_PROFESSOR";
        databaseutil.getConexaoDataBase().execSQL(sql);
        StringBuilder sqlddl = new StringBuilder();
        sqlddl.append("CREATE TABLE TB_PROFESSOR(");
        sqlddl.append("ID_PROFESSOR INTEGER PRIMARY KEY AUTOINCREMENT,");
        sqlddl.append("Nome TEXT NOT NULL, ");
        sqlddl.append("CPF TEXT, ");
        sqlddl.append("EMAIL TEXT NOT NULL, ");
        sqlddl.append("SENHA TEXT, ");
        sqlddl.append("CEP TEXT, ");
        sqlddl.append("ESTADO TEXT, ");
        sqlddl.append("CIDADE TEXT, ");
        sqlddl.append("LOGRADOURO TEXT, ");
        sqlddl.append("NUMERO TEXT, ");
        sqlddl.append("BAIRRO TEXT, ");
        sqlddl.append("TELEFONE TEXT, ");
        sqlddl.append("SEXO TEXT) ");
        databaseutil.getConexaoDataBase().execSQL(sqlddl.toString());
    }

    public void criaDisciplinaProfessor(){
        String sql = "DROP TABLE IF EXISTS TB_PROFESSOR_DISCIPLINA";
        databaseutil.getConexaoDataBase().execSQL(sql);
        StringBuilder sqlddl = new StringBuilder();
        sqlddl.append("CREATE TABLE TB_PROFESSOR_DISCIPLINA(");
        sqlddl.append("ID_PROFESSOR_DISCIPLINA INTEGER PRIMARY KEY AUTOINCREMENT,");
        sqlddl.append("SERIE TEXT NOT NULL,");
        sqlddl.append("DISCIPLINA TEXT NOT NULL)");
        databaseutil.getConexaoDataBase().execSQL(sqlddl.toString());
    }

    private ContentValues carregaProfessor(Professor professor){
        ContentValues cv = new ContentValues();
        cv.put("Nome", professor.getNome());
        cv.put("CPF", professor.getCpf());
        cv.put("EMAIL", professor.getEmail());
        cv.put("SENHA", professor.getSenha());
        cv.put("CEP", professor.getCep());
        cv.put("ESTADO",professor.getEstado());
        cv.put("CIDADE", professor.getCidade());
        cv.put("LOGRADOURO", professor.getEndereco());
        cv.put("NUMERO", professor.getNumero());
        cv.put("BAIRRO", professor.getBairro());
        cv.put("TELEFONE", professor.getTelefone());
        cv.put("SEXO", professor.getSexo());
        return cv;
    }

    private ContentValues carregaProfessorDisciplina(DisciplinaProfessor disciplinaProfessor){
        ContentValues cv = new ContentValues();
        cv.put("SERIE", disciplinaProfessor.getSerie());
        cv.put("DISCIPLINA", disciplinaProfessor.getDisciplina());
        return cv;
    }

    private ContentValues carregaPerfil(String string){
        ContentValues cv = new ContentValues();
        cv.put("Perfil",string);
        return  cv;
    }

    private ContentValues carregaAluno(Aluno aluno, String id){
        ContentValues cv = new ContentValues();
        cv.put("ID_ALUNO", id);
        cv.put("Nome",aluno.getNome());
        cv.put("Email", aluno.getEmail());
        cv.put("Escola", aluno.getEscola());
        cv.put("Matricula",aluno.getMatricula());
        cv.put("Serie", aluno.getSerie());
        cv.put("Tipo", aluno.getTipo());
        return cv;
    }




    public void salvaPerfil(String perifl){
        databaseutil.getConexaoDataBase().insert("TB_Perfil",null, carregaPerfil(perifl));
    }

    public void salvaAluno(Aluno aluno, String id){
        databaseutil.getConexaoDataBase().insert("TB_ALUNO",null, carregaAluno(aluno,id));
    }

    public void salvaProfessor(Professor professor){
        databaseutil.getConexaoDataBase().insert("TB_PROFESSOR", null, carregaProfessor(professor));
    }

    public void salvaDisciplinaProfessor(DisciplinaProfessor disciplinaProfessor){
        databaseutil.getConexaoDataBase().insert("TB_PROFESSOR_DISCIPLINA", null, carregaProfessorDisciplina(disciplinaProfessor));
    }


    public String getPerfil(){
        String sql = "SELECT * FROM TB_Perfil";
        Cursor cursor = databaseutil.getConexaoDataBase().rawQuery(sql,null);
        cursor.moveToFirst();
        return cursor.getString(cursor.getColumnIndex("Perfil"));
    }

    public List<Aluno> getAluno(){
        List<Aluno> aluno = new ArrayList<>();
        String sql = "SELECT * FROM TB_ALUNO";
        Cursor cursor = databaseutil.getConexaoDataBase().rawQuery(sql,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            aluno.add(carregaAluno(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return aluno;
    }

    public List<Professor> getProfessor(){
        List<Professor> professor = new ArrayList<>();
        String sql = "SELECT * FROM TB_PROFESSOR";
        Cursor cursor = databaseutil.getConexaoDataBase().rawQuery(sql,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            professor.add(carregaProfessor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return professor;
    }

    public List<DisciplinaProfessor> getDisciplinaProfessor(){
        List<DisciplinaProfessor> disciplinaProfessors = new ArrayList<>();
        String sql = "SELECT * FROM TB_PROFESSOR_DISCIPLINA";
        Cursor cursor = databaseutil.getConexaoDataBase().rawQuery(sql,null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            disciplinaProfessors.add(carregaDP(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return disciplinaProfessors;
    }

    public String getIDAluno(){
        String sql = "SELECT ID_ALUNO FROM TB_ALUNO";
        Cursor cursor = databaseutil.getConexaoDataBase().rawQuery(sql,null);
        cursor.moveToFirst();
        return cursor.getString(cursor.getColumnIndex("ID_ALUNO"));
    }

    private Aluno carregaAluno(Cursor cursor){
        Aluno aluno = new Aluno();
        aluno.setNome(cursor.getString(cursor.getColumnIndex("Nome")));
        aluno.setEmail(cursor.getString(cursor.getColumnIndex("Email")));
        aluno.setEscola(cursor.getString(cursor.getColumnIndex("Escola")));
        aluno.setMatricula(cursor.getString(cursor.getColumnIndex("Matricula")));
        aluno.setSerie(cursor.getString(cursor.getColumnIndex("Serie")));
        aluno.setTipo(cursor.getString(cursor.getColumnIndex("Tipo")));
        return aluno;
    }

    private Professor carregaProfessor(Cursor cursor){
        Professor professor = new Professor();
        professor.setNome(cursor.getString(cursor.getColumnIndex("Nome")));
        professor.setCpf(cursor.getString(cursor.getColumnIndex("CPF")));
        professor.setEmail(cursor.getString(cursor.getColumnIndex("EMAIL")));
        professor.setSenha(cursor.getString(cursor.getColumnIndex("SENHA")));
        professor.setCep(cursor.getString(cursor.getColumnIndex("CEP")));
        professor.setEstado(cursor.getString(cursor.getColumnIndex("ESTADO")));
        professor.setCidade(cursor.getString(cursor.getColumnIndex("CIDADE")));
        professor.setEndereco(cursor.getString(cursor.getColumnIndex("LOGRADOURO")));
        professor.setNumero(cursor.getString(cursor.getColumnIndex("NUMERO")));
        professor.setBairro(cursor.getString(cursor.getColumnIndex("BAIRRO")));
        professor.setTelefone(cursor.getString(cursor.getColumnIndex("TELEFONE")));
        professor.setSexo(cursor.getString(cursor.getColumnIndex("SEXO")));
        return professor;
    }

    private DisciplinaProfessor carregaDP(Cursor cursor){
        DisciplinaProfessor disciplinaProfessor = new DisciplinaProfessor();
        disciplinaProfessor.setDisciplina(cursor.getString(cursor.getColumnIndex("DISCIPLINA")));
        disciplinaProfessor.setSerie(cursor.getString(cursor.getColumnIndex("SERIE")));
        return disciplinaProfessor;
    }








}
