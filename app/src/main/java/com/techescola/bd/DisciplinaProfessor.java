package com.techescola.bd;

public class DisciplinaProfessor {
    private String serie;
    private String disciplina;

    public DisciplinaProfessor(){

    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }
}
